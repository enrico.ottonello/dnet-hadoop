
package eu.dnetlib.dhp.bulktag.criteria;

public interface Selection {

	boolean apply(String value);
}
