
package eu.dnetlib.dhp.actionmanager.project.utils;

import java.io.Serializable;

/**
 * the mmodel for the projects csv file
 */
public class CSVProject implements Serializable {
	private String rcn;
	private String id;
	private String acronym;
	private String status;
	private String programme;
	private String topics;
	private String frameworkProgramme;
	private String title;
	private String startDate;
	private String endDate;
	private String projectUrl;
	private String objective;
	private String totalCost;
	private String ecMaxContribution;
	private String call;
	private String fundingScheme;
	private String coordinator;
	private String coordinatorCountry;
	private String participants;
	private String participantCountries;
	private String subjects;

	public String getRcn() {
		return rcn;
	}

	public void setRcn(String rcn) {
		this.rcn = rcn;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProgramme() {
		return programme;
	}

	public void setProgramme(String programme) {
		this.programme = programme;
	}

	public String getTopics() {
		return topics;
	}

	public void setTopics(String topics) {
		this.topics = topics;
	}

	public String getFrameworkProgramme() {
		return frameworkProgramme;
	}

	public void setFrameworkProgramme(String frameworkProgramme) {
		this.frameworkProgramme = frameworkProgramme;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getProjectUrl() {
		return projectUrl;
	}

	public void setProjectUrl(String projectUrl) {
		this.projectUrl = projectUrl;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public String getEcMaxContribution() {
		return ecMaxContribution;
	}

	public void setEcMaxContribution(String ecMaxContribution) {
		this.ecMaxContribution = ecMaxContribution;
	}

	public String getCall() {
		return call;
	}

	public void setCall(String call) {
		this.call = call;
	}

	public String getFundingScheme() {
		return fundingScheme;
	}

	public void setFundingScheme(String fundingScheme) {
		this.fundingScheme = fundingScheme;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	public String getCoordinatorCountry() {
		return coordinatorCountry;
	}

	public void setCoordinatorCountry(String coordinatorCountry) {
		this.coordinatorCountry = coordinatorCountry;
	}

	public String getParticipants() {
		return participants;
	}

	public void setParticipants(String participants) {
		this.participants = participants;
	}

	public String getParticipantCountries() {
		return participantCountries;
	}

	public void setParticipantCountries(String participantCountries) {
		this.participantCountries = participantCountries;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

}
